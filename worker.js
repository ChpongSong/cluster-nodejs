const http = require('http');
const port = 8000;
const pid = process.pid;


const server = http.createServer((req, res) => {
    res.end('Hello world!\n');
}).listen(port, () => {
    console.log(`Worker has been started in ${port},Pid : ${pid}`);
});

process.on('SIGINT', () => {
    console.log('Signal is SIGINT');
    server.close(() => {
        process.exit(0);
    });
});

process.on('SIGTERM', () => {
    console.log('Signal is SIGTERM');
    server.close(() => {
        process.exit(0);
    });
});

process.on('SIGUSR2', () => {
    console.log('Signal is SIGUSR2');
    server.close(() => {
        process.exit(1);
    });
});